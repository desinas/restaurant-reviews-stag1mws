let CACHE_NAME = 'restaurant-reviews-v1';
let urls2cache = [
  '/',
  'index.html',
  'restaurant.html',
  'css/styles.css',
  'js/main.js',
  'js/restaurant_info.js',
  'js/dbhelper.js',
  'js/indexController.js',
  'data/restaurants.json',
  'img/1.jpg',
  'img/2.jpg',
  'img/3.jpg',
  'img/4.jpg',
  'img/5.jpg',
  'img/6.jpg',
  'img/7.jpg',
  'img/8.jpg',
  'img/9.jpg',
  'img/10.jpg',
];

/**
* Installing SW by caching responses to the request for site assets
*/
self.addEventListener('install', function(event) {

  event.waitUntil(caches.open(CACHE_NAME).then(function(cache) {

  	  return cache.addAll(urls2cache);
  }));
});

/**
* Activating SW check the current cache and delete old version
*/
self.addEventListener('activate', function(event) {

  console.log("Activating the new SW by replacing the previous.");

  event.waitUntil(caches.keys().then(function(cacheNames) {

    return Promise.all(cacheNames.map(function(cache) {
      if (CACHE_NAME.indexOf(cache) === -1) {
          return caches.delete(cache);
      }
    }));
  }));
});

/**
* Fetching the site assets by
* checking if restaurant details is the request and responds to it,
* if it is not, for all other requests respond according the needs
*/
self.addEventListener('fetch', function(event) {

  if (event.request.url.includes('restaurant.html')) {

  	caches.match('restaurant.html').then(function(response) {
  	  return response;
  	});
  }

  event.respondWith(caches.match(event.request).then(function(response) {

      return response || fetch(event.request);
  }));
});
