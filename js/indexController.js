/**
* Check if the running browser supports service worker
* and registers it to work on the root of the application
*/
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw.js', {scope: '/'}).then( (reg) => {
      console.log("Service Worker installed")
    }).catch( (err) => {
      console.log("Service Worker failed: ", err);
    });
  });
}
